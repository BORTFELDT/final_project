# final_project

## AOS573 Fall 2023 Final Project

## Description
This repository contains three small datasets: total number of wildfires and burn acreage in the US between 1983 and 2022, monthly in situ global mean atmospheric CO2 concentration between 1979 and mid-2023, and monthly global mean precipitation between 1979 and mid-2023. 

This repository also contains an environment file for the purposes of building a link via mybinder.org.

Finally, this repository contains a Jupyter notebook in which all statistical analyses, plotting, and written details were conducted for this final project: "Investigating the Correlation between Global CO2 Concentration and Wildfire Severity in the United States"
